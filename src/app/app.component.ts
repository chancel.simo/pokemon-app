import { Component, ElementRef, HostListener, OnInit } from '@angular/core';

import { POKEMON } from './list-pokemon';
import { Pokemon } from './pokemon';

@Component({
    selector: 'pokemon-app',
    templateUrl: './app/app.component.html',
    styleUrls: ['./app/app.component.scss'],
})

export class AppComponent{ }


