import { Directive, ElementRef, HostListener, Input } from "@angular/core";

@Directive({
  selector:'[pkmnBorderCard]'  
})

export class BorderCardDirective{
    constructor(private el: ElementRef){
        this.setBorder('#f5f5f5');
        this.setHeight(250);
    }

    @Input('pkmnBorderCard') borderColor: string;

    private setBorder(color: string){
        let border = 'solid 4px ' + color;
        this.el.nativeElement.style.border = border;
    }

    private setHeight(height: number){
        this.el.nativeElement.style.height = height+ 'px';
    }

    @HostListener('mouseenter') onMouseEnter(){
        this.setBorder(this.borderColor || 'green');
    }

    @HostListener('mouseleave') onMouseLeave(){
        this.setBorder('#f5f5f5');
    }

    private setLog(msg: string){
        console.log(msg);
    }
}