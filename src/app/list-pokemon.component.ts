import { Router } from '@angular/router';
import { Component, ElementRef, HostListener, OnInit } from '@angular/core';

import { POKEMONS } from './list-pokemon';
import { Pokemon } from './pokemon';

@Component({
    selector: 'list-pokemon',
    templateUrl: './app/list-pokemon.component.html',
    styleUrls: ['./app/app.component.scss'],
})

export class ListPokemonComponent implements OnInit { 

    name = 'pokemon';
    isTest: boolean;
    constructor(private router: Router){

    }
    private pokemons : Pokemon[];

    ngOnInit(){
            this.pokemons = POKEMONS;
    }

    selectPokemon(pokemon: Pokemon){
        let link = ['/pokemon', pokemon.id];
        this.router.navigate(link);   
    }

    @HostListener('mouseenter') onMouseEnter(){
        this.isTest = true;
    }

    @HostListener('mouseleave') onMouseLeave(){
        this.isTest = false;
    }

}


