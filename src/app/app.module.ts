import { ListPokemonComponent } from './list-pokemon.component';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
  
import { AppComponent }  from './app.component';
import { BorderCardDirective } from './border-card.directive';
import { PokemonTypeColorPipe } from './pokemon-type-color.pipe';
import { DetailPokemonComponent } from './detail-pokemon.component';
import { AppRoutingModule } from './app-routing.module';
  
@NgModule({
  imports:      
  [ 
      BrowserModule ,
      AppRoutingModule,
  ],
  declarations: [ 
      AppComponent,
      ListPokemonComponent,
      DetailPokemonComponent,
      BorderCardDirective,
      PokemonTypeColorPipe
  ],
  bootstrap:
  [ 
     AppComponent 
  ]
})
export class AppModule { }